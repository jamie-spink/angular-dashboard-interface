import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CanvasCellComponent } from './canvas-cell.component';

describe('CanvasCellComponent', () => {
  let component: CanvasCellComponent;
  let fixture: ComponentFixture<CanvasCellComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CanvasCellComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CanvasCellComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
