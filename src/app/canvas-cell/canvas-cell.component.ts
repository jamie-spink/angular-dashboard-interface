import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { Server, App, createServer } from '../interfaces';

@Component({
  selector: 'app-canvas-cell',
  templateUrl: './canvas-cell.component.html',
  styleUrls: ['./canvas-cell.component.scss'],
})
export class CanvasCellComponent implements OnInit {

  @Input() server: Server;

  @Output() grandChildKillServer = new EventEmitter();

  constructor() {}

  ngOnInit() {}

  killServer(): void {
    this.grandChildKillServer.emit();
  }

}
