export interface App {
    Name: string;
    Abbreviation: string;
    Color: string;
    Time?: Date;
    CurrentServerId?: number;
}

export interface Server {
    Id: number;
    Apps: App[];
}
  
export function createServer(state: Server[]): Server {
    return {
        Id: state.length + 1,
        Apps: []
    }
}

export function createApp(Name:string,Abbreviation:string,Color?:string,Time?:Date,CurrentServerId?:number): App {
    return {
        Name,
        Abbreviation,
        Color,
        Time,
        CurrentServerId,
    }
}