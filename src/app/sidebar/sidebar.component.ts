import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Server, App, createServer } from '../interfaces';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit {

  @Input() apps: App[];

  @Output() eventAddServer = new EventEmitter();

  @Output() eventRemoveServer = new EventEmitter();

  @Output() eventAddApp = new EventEmitter();

  @Output() eventRemoveApp = new EventEmitter();

  constructor() {}

  ngOnInit() {}

  addServer(event: any): void {
    this.eventAddServer.emit(event);
  }

  removeServer(event: any): void {
    this.eventRemoveServer.emit(event);
  }

  addApp(event: any): void {
    this.eventAddApp.emit(event);
  }

  removeApp(event: any): void {
    this.eventRemoveApp.emit(event);
  }

}
