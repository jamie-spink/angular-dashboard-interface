import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Server, App, createServer } from '../interfaces';

@Component({
  selector: 'app-canvas',
  templateUrl: './canvas.component.html',
  styleUrls: ['./canvas.component.scss']
})
export class CanvasComponent implements OnInit {

  server: Server;

  @Input() state: Server[];

  @Output() parentKillServer = new EventEmitter<Server>();

  constructor() {}

  ngOnInit() {}

  childKillServer(server) {
    this.server = server;
    this.parentKillServer.emit(this.server);
  }

}
