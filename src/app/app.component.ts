import { Component, OnInit } from '@angular/core';
import { Server, App, createServer, createApp } from './interfaces';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  
  serverState:Server[] = [
    { 
      Id: 1,
      Apps: []
    },
    {
      Id: 2,
      Apps: []
    },
    {
      Id: 3,
      Apps: []
    },
    {
      Id: 4,
      Apps: []
    }
  ]

  apps:App[] = [
    {
      Name: "Hadoop",
      Abbreviation: "Hd",
      Color: "#e805bf"
    },
    {
      Name: "Rails",
      Abbreviation: "Ra",
      Color: "#5a26e5"
    },
    {
      Name: "Chronos",
      Abbreviation: "Ch",
      Color: "#019dfe"
    },
    {
      Name: "Storm",
      Abbreviation: "St",
      Color: "#24deae"
    },
    {
      Name: "Spark",
      Abbreviation: "Sp",
      Color: "#65eb26"
    }
  ] 

  childEventAddServer(event: any) {
    this.serverState.push(createServer(this.serverState));
  }

  childEventRemoveServer(event: any) {
    if(this.serverState.length > 0) {
      var removed = this.serverState.splice(-1);
      for(var i = 0; i < removed[0].Apps.length; i++) {
        this.childEventAddApp(removed[0].Apps[i]);
      }
    }
  }

  childEventAddApp(app: App) {
    let firstZero = false;
    let firstOne = false;
    for(var i = 0; i < this.serverState.length; i++) {
      if(this.serverState[i].Apps.length == 0) {
        this.serverState[i].Apps.push(createApp(app.Name,app.Abbreviation,app.Color,new Date(),this.serverState[i].Id));
        firstZero = true;
        return;
      }
    }
    if(firstZero == false) {
      for(var i = 0; i < this.serverState.length; i++) {
        if(this.serverState[i].Apps.length == 1) {
          this.serverState[i].Apps.push(createApp(app.Name,app.Abbreviation,app.Color,new Date(),this.serverState[i].Id));
          firstOne = true;
          return;
        }
      }
    }
  }

  childEventRemoveApp(app: App) {
    this.updateAppServerIds();
    var instances = [];
    for(var i = 0; i < this.serverState.length; i++) {
      for(var j = 0; j < this.serverState[i].Apps.length; j++) {
        if(this.serverState[i].Apps[j].Name == app.Name) {
          instances.push(this.serverState[i].Apps[j]);
        }
      }
    }
    if(instances.length > 0) {
      var sortedInstances = instances.sort(function (a, b) {
        return b.Time - a.Time;
      });
      var instanceToRemove = sortedInstances[0];
      var serverIndex = instanceToRemove.CurrentServerId - 1;
      for(var i = 0; i < this.serverState[serverIndex].Apps.length; i++) {
        if (this.serverState[serverIndex].Apps[i].Name == instanceToRemove.Name) {
          this.serverState[serverIndex].Apps.splice(i,1);
          return;
        }
      }
    }
  }

  killServer(server) {
    for(var i = 0; i < this.serverState.length; i++) {
      if(this.serverState[i].Id == server.Id) {
        var removed = this.serverState.splice(i ,1);
        break;
      }
    }
  }

  updateAppServerIds() {
    for(var i = 0; i < this.serverState.length; i++) {
      for(var j = 0; j < this.serverState[i].Apps.length; j++) {
        this.serverState[i].Apps[j].CurrentServerId = (i + 1);
      }
    }
  }

  ngOnInit() {}

}
