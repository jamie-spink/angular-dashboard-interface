# AngularDashboardInterface

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 6.1.3.

## Development server

Run `npm install` and then `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## 3rd party libraries

Aside from the core angular components, the only 3rd party library I have used is time-ago-pipe, which can be found here: https://www.npmjs.com/package/time-ago-pipe. I used it because I deemed it necessary to abstract away the functionality for showing the time since an app was added to a server. This is mostly due to time constraints, but also due to the fact that functionalities of this nature can be unnecessarily complex in Angular and very difficult to get right.

## Notes

I have taken the time to optimize the layout for mobile devices (to the extent afforded to me by my limited time available), and I have also added the functionality to be able to destroy a server by clicking directly on it within the canvas. 

